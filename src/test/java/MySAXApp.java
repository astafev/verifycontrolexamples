import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileReader;
import java.io.IOException;
import org.xml.sax.Attributes;

/**
 * Created with IntelliJ IDEA.
 * Date: 10.09.12
 * Time: 13:11
 *
 * @author astafev (Astafyev Evgeny)
 */
public class MySAXApp extends DefaultHandler {

    public MySAXApp() {
        super();
    }

    public static void main(String[] args) throws SAXException, ParserConfigurationException, IOException {
        XMLReader xr = XMLReaderFactory.createXMLReader();
        MySAXApp handler = new MySAXApp();
        xr.setContentHandler(handler);
        xr.setErrorHandler(handler);

        FileReader fileReader = new FileReader("out.txt");
        xr.parse(new InputSource(fileReader));
    }

    public void launch() throws SAXException, ParserConfigurationException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();

        parser.parse("out.txt", this);


    }

    public void startDocument() {
        System.out.println("Started parsing");
    }



    public void startElement(String uri, String name, String qName, Attributes atts){
        System.out.println("started element:\n  uri: " + uri +
        "\n  qName: " + qName +
        "\n  name: " + name +
        "\n  atts: " + atts.getLength());
    }

    public void characters(char[] ch,
                           int start,
                           int length){
        System.out.println(ch);
        System.out.println(start);
        System.out.println(length);
    }

}

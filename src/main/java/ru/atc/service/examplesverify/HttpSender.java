package ru.atc.service.examplesverify;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;

import java.io.*;

/**
 * Посылает запросы на сервер, возвращаемое значение - HttpResponse.
 * @date 03.09.2012
 * @author astafev (Astafyev Evgeny)
 * @version 0.1
 */
public class HttpSender {

    static private String FLCUri = "http://192.168.100.96:8080/smev/Flc";
    static private String signatureUri = "http://192.168.100.96:7777/gateway/services/SID0003038";
    static private String certificateUri = "http://192.168.100.96:7777/gateway/services/SID0003064";
    static private Logger log = ExamplesVerifier.log;
    String certificate = "";

    private String message;

    static private HttpClient client = new DefaultHttpClient();

    public HttpSender(String fileName) throws IOException {
        this(new File(fileName));
    }

    public HttpSender(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        byte[] bytes = new byte[fis.available()];
        fis.read(bytes);
        message = new String(bytes);
    }


    /**
     * Берет параметра адресов сервисов. Надо прикрутить Properties.
     * */
    public static void getParamters(){
        //todo
    }

    static HttpClient getClient(){
        return client;
    }

    /**
     * Отправляет на сервис проверки метод.рекомендаций*/
    public HttpResponse sendToFLC () throws IOException {
        String firstPart = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://smev.gosuslugi.ru/flc/types\">\n" +
                "<soapenv:Header/>" +
                "<soapenv:Body>" +
                "<typ:FlcRequest>\n" +
                "<message><![CDATA[";
        //todo предусмотреть возможно разных метод-рекомендаций
        String lastPart = "]]></message>" +
                "<configId>rev111111</configId>" +
                "<checkLevel>0</checkLevel>" +
                "<isSilent>false</isSilent>" +
                "<serviceId>SID0003001</serviceId>" +
                "<smevId>pk3smev</smevId>" +
                "<msgId>c6a0d5c2-f3c4-1fe9-be59-c15a39c1cded</msgId>" +
                "</typ:FlcRequest>" +
                "</soapenv:Body>" +
                "</soapenv:Envelope>";
        return sendRequest(firstPart,lastPart, "http://smev.gosuslugi.ru/flc/FlcApply", FLCUri);
    }


    public HttpResponse sendToSignature () throws IOException {
        String firstPart = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://smev.gosuslugi.ru/SignatureTool/xsd/\">" +
                "<soapenv:Header/>" +
                "<soapenv:Body>" +
                "<xsd:VerifySignatureRequest>" +
                "<message><![CDATA[";
        String lastPart = "]]></message>" +
                "<isCertCheck>true</isCertCheck>" +
                "<actor>http://smev.gosuslugi.ru/actors/smev</actor>" +
                "</xsd:VerifySignatureRequest>" +
                "</soapenv:Body>" +
                "</soapenv:Envelope>";
        return sendRequest(firstPart,lastPart, "http://smev.gosuslugi.ru/SignatureTool/verifySignature", signatureUri);
    }

    /**
     *
     * */
    //todo: not ready yet
    public HttpResponse sendToCertificate() throws IOException {
        String firstPart = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:esv=\"http://esv.server.rt.ru\">\n" +
                "<soapenv:Header/>" +
                "<soapenv:Body>" +
                "<esv:VerifyCertificate>" +
                "<esv:certificate>";
        String lastPart = "]]></message>" +
                "<isCertCheck>true</isCertCheck>" +
                "<actor>http://smev.gosuslugi.ru/actors/smev</actor>" +
                "</xsd:VerifySignatureRequest>" +
                "</soapenv:Body>" +
                "</soapenv:Envelope>";

        HttpPost request = new HttpPost(certificateUri);
        request.addHeader("SOAPAction", "http://esv.server.rt.ru/VerifyCertificate");
        HttpEntity entity = new StringEntity(firstPart + certificate + lastPart, ContentType.create("text/xml","UTF-8"));
        request.setEntity(entity);
        return client.execute(request);
    }

    private HttpResponse sendRequest(String firstPart, String lastPart, String soapAction, String URI) throws IOException {
        HttpPost request = new HttpPost(URI);
        request.addHeader("SOAPAction", soapAction);
        HttpEntity entity = new StringEntity(firstPart + message + lastPart, ContentType.create("text/xml","UTF-8"));
        request.setEntity(entity);
        HttpResponse response = client.execute(request);
        //нужно ли закрывать соеднинение и как это делать?
        return response;
    }


}

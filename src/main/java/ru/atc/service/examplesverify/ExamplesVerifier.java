package ru.atc.service.examplesverify;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.conn.params.ConnRoutePNames;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Главный класс. Надо чтоб мутил всё...
 * @author astafev (Astafyev Evgeny)
 */
public class ExamplesVerifier {

    static org.slf4j.Logger log = LoggerFactory.getLogger(ExamplesVerifier.class.getName());
    static StringBuffer report = new StringBuffer("<head><meta charset=\"utf-8\"/><title>Проверка, ёпта!</title></head><body><h1>Отчет, мазафака!</h1><p><table border=\"3\"><tr>" +
            "<th>Имя файла</th>" +
            "<th>Проверка метод рекомендаций</th>" +
            "<th>Проверка подписи</th></tr>");

    static String oldReports = "old_reports";

    public static SimpleDateFormat dateFromat = new SimpleDateFormat("dd-MM-yyyy ' T ' HH-mm-ss");
    private static boolean TO_USE_PROXY = true;


    //возможно потом стоит убрать исключение из сигнатуры метода
    public static void main(String[] args) throws SAXException {
        //TODO написать инструкцию
        XMLReader xr = XMLReaderFactory.createXMLReader();
        XMLHandler handler = new XMLHandler();
        xr.setContentHandler(handler);
        xr.setErrorHandler(handler);

        File requestsDirectory = new File("requests");
        if(!requestsDirectory.exists() || !requestsDirectory.isDirectory()) {
            log.error("Didn't find directory 'requests'. Shutting down...");
            System.err.println("Didn't find directory 'requests'. Shutting down...");
            report.append("<span style=\"color:red;\">ERROR:</span> Didn't find directory 'requests'!");
            createReport();
            return;
        }
        File[] requests = requestsDirectory.listFiles();
        //у нас есть куча файликов, надо прогнать их по тестам
        for(File request:requests) {
            try {
                report.append("<tr>");
                HttpSender sender = new HttpSender(request);
                if(TO_USE_PROXY)
                    HttpSender.getClient().getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, new HttpHost("localhost", 8888)); //прокси
                report.append("<td>" + request.getName() + "</td>");

                InputStream inputStream = sender.sendToFLC().getEntity().getContent();
                xr.parse(new InputSource(inputStream));

                inputStream = sender.sendToSignature().getEntity().getContent();
                xr.parse(new InputSource(inputStream));

                // //certificate - сертификат
                //todo умно логгируем, формируем отчет

                if (false/*если подпись верна*/) {
                    //смотрим //Code, должен быть ноль, ответ //VerifyCertificateResult
                    HttpResponse certificateResponse = sender.sendToCertificate();
                    //todo умно логгируем, формируем отчет
                } else {
                    //todo умно логгируем, формируем отчет
                }
                report.append("</tr>");

            } catch (IOException e) {
                log.error("Something wrong...", e);
            }
        }
        createReport();
    }

    /**
     * Пишет отчет составленный к этому моменту в файл.
     * */
    private static void createReport() {
        report.append("</table></p></body>");
        File reportFile = new File("report.html");
        try {
            if(reportFile.exists())
            {
                File oldReportsDirectory = new File(oldReports);
                oldReportsDirectory.mkdir();
                File f = new File(oldReports + "/" + dateFromat.format(new Date()) + ".html");
                reportFile.renameTo(f);
                log.info("Old file 'report.html' moved to '" + f.getAbsolutePath() + "'.");
                reportFile.createNewFile();

            }

            FileWriter writer = new FileWriter(reportFile);
            writer.write(report.toString());
            writer.close();
            log.info("Report writed in " + reportFile.getName());

        } catch (IOException e) {
            System.err.println("Couldn't create file 'report.html'! Putting to console...");
            System.out.println("\n\n---------------REPORT--------------\n" + report);
            e.printStackTrace();
        }

    }

    /**
     * //TODO дописать errorHandler
     * */
    static class XMLHandler extends DefaultHandler {
        // b=0 не дошли, b=1 - дошли, b=2 - все ок, b=3 - ошибка
        byte b;

        @Override
        public void startDocument(){

            b=0;
        }

        @Override
        public void characters(char[] ch,
                               int start,
                               int length){
            if(b==1) {
                if( length==1 && ch[start] == '0'){
                    //код ошибки ноль
                    report.append("<td style=\"background-color: rgb(0, 255, 0);\">OK!");
                    b++;
                }
                else{
                    report.append("<td style=\"background-color: rgb(255, 0, 0);\"><b>errorCode</b>");
                    b=3;
                }
            }
            if(b==3) {
                report.append(": ");
                report.append(ch, start, length);
            }

        }

        @Override
        public void startElement(String uri, String name, String qName, Attributes atts) {
            if (name.equals("errorCode")) {
                b=1;
            }
            if(name.equals("Fault")) {
                b=3;
                report.append("<td style=\"background-color: rgb(255, 0, 0);\">");
            }
            if(b==3) {
                report.append("<br><b>" + name + "</b>");
            }
        }

        @Override
        public void endDocument(){
            report.append("</td>");
        }

    }

}




/**
 * <env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
 <env:Header/>
 <env:Body>
 <ns1:FlcResponse xmlns:ns2="http://idecs.nvg.ru/identityservice/ws/types/" xmlns:ns1="http://smev.gosuslugi.ru/flc/types">
 <error>
 <ns2:errorCode>0</ns2:errorCode>
 </error>
 </ns1:FlcResponse>
 </env:Body>
 </env:Envelope>*/
